class MicropostsController < ApplicationController
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_user, only: :destroy

  def index
  end

  def create
    #@micropost = current_user.microposts.build(params[:micropost])
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:sucess] = "Micropost created!"
      redirect_to root_path_url
    else
      @feed_items = []
      render 'static_pages/home'
    end

  end

  def destroy
    @micropost.destroy
    redirect_back_or root_path_url
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def micropost_params
      params.require(:micropost).permit(:content, :user_id)#, :create_remember_token
    end

    def correct_user
      @micropost = current_user.microposts.find_by_id(params[:id])
      redirect_to(root_path_url) if @micropost.nil?
    end

    def correct_user_exception
      @micropost = current_user.microposts.find_by_id(params[:id])
    rescue
      redirect_to(root_path_url)
    end


end
