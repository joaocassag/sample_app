Rails.application.routes.draw do
  #get 'users/new'
  #match 'users_new', to: 'users#new', via: [:get], as: 'users_new_path'
  resources :users

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :sessions, only: [:new, :create, :destroy]
  resources :microposts, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]

  match 'signup', to: 'users#new', via: [:get], as: 'signup_path'
  match 'signin', to: 'sessions#new', via: [:get], as: 'signin_path'
  match 'signout', to: 'sessions#destroy', via: [:delete], as: 'signout_path'

  #get 'static_pages/home'
  #match 'home', to: 'static_pages#home', via: [:get], as: 'root_path'
  root to: 'static_pages#home', as: 'root_path'

  #get 'static_pages/help'
  match 'help', to: 'static_pages#help', via: [:get], as: 'help_path'
  #match 'help', to: 'static_pages#help', as: 'help_path'

  #get 'static_pages/about'
  match 'about', to: 'static_pages#about', via: [:get], as: 'about_path'

  #get 'static_pages/contact'
  match 'contact', to: 'static_pages#contact', via: [:get], as: 'contact_path'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
